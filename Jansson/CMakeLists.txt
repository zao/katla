cmake_minimum_required(VERSION 2.8)

file(GLOB JANSSON_SOURCES src/*.c)
include_directories(src .)
add_library(Jansson STATIC ${JANSSON_SOURCES})
