// Copyright Lars Viklund 2012.
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

// vim: ts=4 sw=4 et cin

#include <cstring>
#include <deque>
#include <iostream>
#include <ostream>
#include <map>
#include <set>
#include <string>
#include <vector>

#include <stb_image.h>
#include <jansson.h>
#include <MaxRectsBinPack.h>

#include <glob.h>

#include "info.h"

void Usage();

struct AlgorithmInput
{
    std::vector<ImageInfo> imageInfos;
    std::string algorithm;
    bool powerOfTwo;
    int maxDimension;
    std::string tilemapBasename;
};

struct AlgorithmOutput
{
    std::vector<TilemapDesc> tilemaps;
};

struct Page
{
    int xCount, yCount;
};

std::ostream& operator << (std::ostream& os, AlgorithmInput const& input);

AlgorithmOutput MAXRECTS_CP(AlgorithmInput input);

AlgorithmOutput Homogeneous(AlgorithmInput input);
void WriteOutput(std::ostream& os, AlgorithmOutput const& out);
void ComputeHomogeneousPages(bool needPoT, int tileCount, int tileW, int tileH, int maxW, int maxH, std::vector<Page>& dims);

int main(int argc, char* argv[])
{
    AlgorithmInput input;
    json_t* p = NULL;
    json_error_t error;

    p = json_loadf(stdin, 0, &error);

    if (! p || ! json_is_object(p))
    {
        std::cerr << "Input missing or does not have a top-level object.\n";
        Usage();
        return 1;
    }

    json_t* algorithm = json_object_get(p, "algorithm"),
          * powerOfTwo = json_object_get(p, "output_power_of_two"),
          * maxDimension = json_object_get(p, "max_dimension"),
          * images = json_object_get(p, "images"),
          * tilemapBasename = json_object_get(p, "tilemap_basename");

    std::vector<std::string> missing;

    if (! algorithm || ! json_is_string(algorithm))
        missing.push_back("algorithm");
    else
    {
        char const* val = json_string_value(algorithm);
        if (! strcmp(val, "homogeneous"))
            input.algorithm = val;
        else if (! strcmp(val, "clb-maxrects-cp"))
            input.algorithm = val;
        else
            missing.push_back("algorithm");
    }

    if (! powerOfTwo || ! (json_is_true(powerOfTwo) || json_is_false(powerOfTwo)))
        missing.push_back("output_power_of_two");
    else
    {
        input.powerOfTwo = json_is_true(powerOfTwo);
    }

    if (! maxDimension || ! json_is_number(maxDimension))
        missing.push_back("max_dimension");
    else
    {
        input.maxDimension = (int)json_number_value(maxDimension);
    }

    std::set<std::string> filenames;

    if (! images || ! json_is_array(images))
        missing.push_back("images");
    else
    {
        size_t n = json_array_size(images);
        for (size_t i = 0; i < n; ++i)
        {
            json_t* elem = json_array_get(images, i);
            if (! json_is_string(elem))
            {
                std::cerr << "Image filename #" << (i+1) << " not a string\n";
                continue;
            }
            char const* pattern = json_string_value(elem);
            glob_t g = {};
            int n = glob(pattern, GLOB_BRACE | GLOB_TILDE_CHECK, NULL, &g);
            filenames.insert(g.gl_pathv, g.gl_pathv+g.gl_pathc);
            globfree(&g);
        }
    }

    for (std::set<std::string>::const_iterator I = filenames.begin(); I != filenames.end(); ++I)
    {
        ImageInfo info = { *I };
        int comp = 0;
        bool validFile = !! stbi_info(info.filename.c_str(), &info.width, &info.height, &info.channels);
        if (validFile)
        {
            if (info.width > input.maxDimension)
            {
                std::cerr << "File \"" << info.filename << "\" has dimensions "
                    << info.width << "x" << info.height << " incompatible with maximum extent "
                    << input.maxDimension << ", skipping it." << std::endl;
                continue;
            }
            input.imageInfos.push_back(info);
        }
        else
        {
            std::cerr << "Missing or invalid file \"" << info.filename << "\"" << std::endl;
        }
    }

    if (! tilemapBasename || ! json_is_string(tilemapBasename))
        missing.push_back("tilemap_basename");
    else
    {
        input.tilemapBasename = json_string_value(tilemapBasename);
    }

    json_decref(p);

    if (! missing.empty())
    {
        Usage();

        for (std::vector<std::string>::const_iterator I = missing.begin();
             I != missing.end();
             ++I)
        {
            std::cerr << "Missing field: " << *I << std::endl;
        }
        std::cerr << std::endl;

        std::cerr << input << std::endl;

        return 2;
    }

    AlgorithmOutput output;
    if (input.algorithm == "homogeneous")
        output = Homogeneous(input);
    else if (input.algorithm == "clb-maxrects-cp")
        output = MAXRECTS_CP(input);
    else
    {
        return 3;
    }

    WriteOutput(std::cout, output);
}

int StreamDump(char const* buffer, size_t size, void* stream)
{
    std::ostream& os = *(std::ostream*)stream;
    os.write(buffer, size);
    return 0;
}

void WriteOutput(std::ostream& os, AlgorithmOutput const& output)
{
    json_error_t error;
    json_t* root = json_object();
    json_t* tilemaps = json_array();
    for (std::vector<TilemapDesc>::const_iterator I = output.tilemaps.begin();
         I != output.tilemaps.end();
         ++I)
    {
        json_t* map = json_object();
        json_object_set_new(map, "filename", json_string(I->filename.c_str()));
        json_object_set_new(map, "width", json_integer(I->width));
        json_object_set_new(map, "height", json_integer(I->height));
        json_object_set_new(map, "channels", json_integer(I->channels));
        json_t* images = json_object();
        for (size_t i = 0; i < I->images.size(); ++i)
        {
            ImageInfo const& info = I->images[i];
            TileInfo const& tile = I->tiles[i];
            json_t* img = json_object();
            json_object_set_new(img, "w", json_integer(info.width));
            json_object_set_new(img, "h", json_integer(info.height));
            json_object_set_new(img, "x", json_integer(tile.x));
            json_object_set_new(img, "y", json_integer(tile.y));
            json_object_set_new(images, info.filename.c_str(), img);
        }
        json_object_set_new(map, "images", images);
        json_array_append_new(tilemaps, map);
    }
    json_object_set_new(root, "tilemaps", tilemaps);

    json_dump_callback(root, &StreamDump, &os, JSON_PRESERVE_ORDER | JSON_INDENT(4));
    json_decref(root);
}


struct Props
{
    int width, height, channels;

    bool operator < (Props const& rhs) const
    {
        if (width != rhs.width) return width < rhs.width;
        if (height != rhs.height) return height < rhs.height;
        return channels < rhs.channels;
    }
};

std::string BuildTilemapPath(std::string const& basename, int ch, int p)
{
    char buf[100] = {};
    sprintf(buf, "-%dch-%d", ch, (int)p+1);
    return basename + buf + ".png";
}

std::string BuildHomogeneousTilemapPath(std::string const& basename, int w, int h, int ch, int p)
{
    char buf[100] = {};
    sprintf(buf, "-%dx%d-%dch-%d", w, h, ch, (int)p+1);
    return basename + buf + ".png";
}

AlgorithmOutput Homogeneous(AlgorithmInput input)
{
    AlgorithmOutput output;
    typedef std::map<Props, std::vector<std::string> > Groupings;
    Groupings groups;
    for (std::vector<ImageInfo>::const_iterator I = input.imageInfos.begin();
         I != input.imageInfos.end();
         ++I)
    {
        Props ps = { I->width, I->height, I->channels };
        groups[ps].push_back(I->filename);
    }
    for (Groupings::const_iterator I = groups.begin();
         I != groups.end();
         ++I)
    {
        int w = I->first.width, h = I->first.height;
        int ch = I->first.channels;
        int tileCount = I->second.size();
        std::vector<Page> dims;
        ComputeHomogeneousPages(input.powerOfTwo, tileCount, w, h, input.maxDimension, input.maxDimension, dims);

        std::vector<std::string>::const_iterator SI = I->second.begin(), SE;
        for (size_t p = 0; p < dims.size(); ++p)
        {
            TilemapDesc desc;
            Page const& page = dims[p];
            desc.filename = BuildHomogeneousTilemapPath(input.tilemapBasename, w, h, ch, p);
            desc.width = page.xCount * w;
            desc.height = page.yCount * h;

            int maxTiles = page.xCount*page.yCount;
            int tilesLeft = std::distance(SI, I->second.end());
            std::advance(SE = SI, std::min(maxTiles, tilesLeft));
            int ix = 0;
            for (;
                    SI != SE;
                    ++SI, ++ix)
            {
                ImageInfo info = { *SI, w, h };
                desc.images.push_back(info);
                TileInfo tile = { w * (ix / page.yCount), h * (ix % page.yCount) };
                desc.tiles.push_back(tile);
            }
            desc.channels = I->first.channels;
            output.tilemaps.push_back(desc);
        }
    }
    return output;
}

int Double(int x) { return x*2; }
int Increment(int x) { return x+1; }

static inline bool IsPowerOfTwo(int n)
{
    return (n & (n-1)) == 0;
}

static inline int RoundUpToPowerOfTwo(int n)
{
    n = n - 1;
    n = n | (n >> 1);
    n = n | (n >> 2);
    n = n | (n >> 4);
    n = n | (n >> 8);
    n = n | (n >> 16);
    return n + 1;
}

void ComputeHomogeneousPages(bool needPoT, int tileCount, int tileW, int tileH, int maxW, int maxH, std::vector<Page>& dims)
{
    int wholeX = maxW / tileW;
    int wholeY = maxH / tileH;
    Page wholePage = { wholeX, wholeY };
    while (tileCount > wholeX*wholeY)
    {
        dims.push_back(wholePage);
        tileCount -= wholeX*wholeY;
    }
    if (tileCount == 0)
        return;

    dims.push_back(wholePage);
    return;

    int bestRows = wholeX , bestCols = wholeY;
    int (*next)(int) = (needPoT ? &Double : &Increment);
    for (int cols = 1; cols <= wholeX; cols = next(cols))
    {
        int rows = tileCount / cols;
        if (needPoT)
            rows = RoundUpToPowerOfTwo(rows);
        if (rows > wholeX)
            continue;
        if (rows*cols < bestRows*bestCols)
        {
            bestRows = rows;
            bestCols = cols;
            if (rows*cols == tileCount)
                break;
        }
    }
    Page page = { bestCols, bestRows };
    dims.push_back(page);
}


struct 
DecreasingWidthDecreasingHeight
{
    bool operator () (ImageInfo const& a, ImageInfo const& b) const
    {
        if (a.width != b.width) return a.width > b.width;
        if (a.height != b.height) return a.height > b.height;
        if (a.channels != b.channels) return a.channels < b.channels;
        return a.filename < b.filename;
    }
};

AlgorithmOutput MAXRECTS_CP(AlgorithmInput input)
{
    AlgorithmOutput output;

    typedef std::set<ImageInfo, DecreasingWidthDecreasingHeight> Group;
    typedef std::map<int, Group> GroupsByChannel;
    GroupsByChannel channelGroups;
    for (std::vector<ImageInfo>::const_iterator I = input.imageInfos.begin();
         I != input.imageInfos.end();
         ++I)
    {
        channelGroups[I->channels].insert(*I);
    }

    for (GroupsByChannel::const_iterator I = channelGroups.begin();
         I != channelGroups.end();
         ++I)
    {
        int ch = I->first;
        Group const& group = I->second;
        int w = input.maxDimension, h = input.maxDimension;
        int serial = 0;
        TilemapDesc descTemplate = {};
        descTemplate.filename = BuildTilemapPath(input.tilemapBasename, ch, serial++);
        descTemplate.width = w;
        descTemplate.height = h;
        descTemplate.channels = ch;

        MaxRectsBinPack::FreeRectChoiceHeuristic heuristic = MaxRectsBinPack::RectContactPointRule;

        std::deque<MaxRectsBinPack> packers;
        std::deque<TilemapDesc> descs;
        packers.push_back(MaxRectsBinPack(input.maxDimension, input.maxDimension));
        descs.push_back(descTemplate);
        for (Group::const_iterator J = group.begin();
             J != group.end();
             ++J)
        {
            bool foundHome = false;
            std::deque<MaxRectsBinPack>::iterator K = packers.begin();
            std::deque<TilemapDesc>::iterator L = descs.begin();
            for (/* initialization intentionally missing */;
                 K != packers.end() && L != descs.end() && !foundHome;
                 ++K, ++L)
            {
                Rect r = K->Insert(J->width, J->height, heuristic, false);
                if (r.height == J->height)
                {
                    TilemapDesc& desc = *L;
                    desc.images.push_back(*J);
                    TileInfo tile = { r.x, r.y };
                    desc.tiles.push_back(tile);
                    foundHome = true;
                }
            }
            if (!foundHome)
            {
                MaxRectsBinPack packer(input.maxDimension, input.maxDimension);
                TilemapDesc desc = descTemplate;
                desc.filename = BuildTilemapPath(input.tilemapBasename, ch, serial++);
                Rect r = packer.Insert(J->width, J->height, heuristic, false);
                desc.images.push_back(*J);
                TileInfo tile = { r.x, r.y };
                desc.tiles.push_back(tile);

                packers.push_back(packer);
                descs.push_back(desc);
            }
        }
        output.tilemaps.insert(output.tilemaps.end(), descs.begin(), descs.end());
    }
    return output;
}

void Usage()
{
    std::cerr << "Input file format - JSON object with the following fields:\n"
        "  algorithm: \"homogeneous | clb-maxrects-cp\" (req)\n"
        "  output_power_of_two: boolean (req)\n"
        "  max_dimension: integer (req)\n"
        "  images: list of files (req)\n"
        "  tilemap_basename: file (req)\n"
        "\n";
}

std::ostream& operator << (std::ostream& os, AlgorithmInput const& input)
{
    os << "Images:";
    for (std::vector<ImageInfo>::const_iterator I = input.imageInfos.begin();
         I != input.imageInfos.end();
         ++I)
    {
        os << " \"" << I->filename << "\"";
    }
    os << (input.imageInfos.empty() ? " none" : "") << std::endl;
    os << "Algorithm: " << input.algorithm << std::endl;
    os << "PowerOfTwo: " << (input.powerOfTwo ? "true" : "false") << std::endl;
    os << "MaxDimension: " << input.maxDimension << std::endl;
    os << "TilemapBasename: " << input.tilemapBasename << std::endl;
}
