#!/usr/bin/env perl

system("mkdir solid");
for ($i = 0x70; $i <= 0xFF; ++$i) {
	for ($j = 0x70; $j < 0xFF; ++$j) {
		$name = sprintf("00%02x%02x", $i, $j);
		system("convert -size 32x32 -define png:bit-depth=8 canvas:#$name solid/$name.png");
	}
}
