// Copyright Lars Viklund 2012.
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

// vim: ts=4 sw=4 et cin

#pragma once

#include <string>
#include <vector>

struct ImageInfo
{
    std::string filename;
    int width, height;
    int channels;
};

struct TileInfo
{
    int x, y;
};

struct TilemapDesc
{
    std::string filename;
    std::vector<ImageInfo> images;
    std::vector<TileInfo> tiles;
    int width, height, channels;
};
