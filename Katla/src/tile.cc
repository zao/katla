// Copyright Lars Viklund 2012.
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

// vim: ts=4 sw=4 et cin

#include <stb_image.h>
#include <stb_image_write.h>
#include <jansson.h>

#include <iostream>
#include <ostream>

#include "info.h"

void Usage();
void RenderTilemap(TilemapDesc desc);

bool debugTint = false;

int main(int argc, char* argv[])
{
    json_t* p;
    json_error_t error;

    for (int i = 1; i < argc; ++i)
    {
        if (argv[i] == std::string("-dt"))
            debugTint = true;
    }

    p = json_loadf(stdin, 0, &error);

    if (! p || ! json_is_object(p))
    {
        std::cerr << "Input missing or doesn't have a top-level object.\n";
        Usage();
        return 1;
    }

    json_t* maps = json_object_get(p, "tilemaps");
    if (! maps || ! json_is_array(maps))
    {
        std::cerr << "\"tilemaps\" element is not an array.\n";
        return 2;
    }

    std::vector<TilemapDesc> tilemaps;
    size_t n = json_array_size(maps);
    for (size_t i = 0; i < n; ++i)
    {
        json_t* map = json_array_get(maps, i);
        if (! json_is_object(map))
        {
            std::cerr << "Tilemap #" << (i+1) << " is not an object.\n";
            continue;
        }
        bool failed = false;
        json_t* filename = json_object_get(map, "filename");
        if (! filename || ! json_is_string(filename))
        {
            std::cerr << "\"filename\" field missing or wrong type for tilemap #" << (i+1) << "\n";
            failed = true;
        }
        json_t* width = json_object_get(map, "width");
        if (! width || ! json_is_number(width))
        {
            std::cerr << "\"width\" field missing or wrong type for tilemap #" << (i+1) << "\n";
            failed = true;
        }
        json_t* height = json_object_get(map, "height");
        if (! height || ! json_is_number(height))
        {
            std::cerr << "\"height\" field missing or wrong type for tilemap #" << (i+1) << "\n";
            failed = true;
        }
        json_t* channels = json_object_get(map, "channels");
        if (! channels || ! json_is_number(channels))
        {
            std::cerr << "\"channels\" field missing or wrong type for tilemap #" << (i+1) << "\n";
            failed = true;
        }
        json_t* images = json_object_get(map, "images");
        if (! images || ! json_is_object(images))
        {
            std::cerr << "\"images\" field missing or wrong type for tilemap #" << (i+1) << "\n";
            failed = true;
        }

        TilemapDesc desc;

        if (!failed)
        {
            desc.width = (int)json_number_value(width);
            desc.height = (int)json_number_value(height);
            desc.channels = (int)json_number_value(channels);
            desc.filename = json_string_value(filename);
        }

        if (!failed)
        {
            char const* imageKey;
            json_t* image;
            json_object_foreach(images, imageKey, image)
            {
                ImageInfo info;
                TileInfo tile;
                if (! json_is_object(image))
                {
                    std::cerr << "\"images\" element \"" << imageKey << "\" is not an object.\n";
                    failed = true;
                    continue;
                }
                json_t* w = json_object_get(image, "w");
                json_t* h = json_object_get(image, "h");
                json_t* x = json_object_get(image, "x");
                json_t* y = json_object_get(image, "y");
                if (! w || ! json_is_number(w) || json_number_value(w) < 1 ||
                    ! h || ! json_is_number(h) || json_number_value(h) < 1 ||
                    ! x || ! json_is_number(x) || json_number_value(x) < 0 ||
                    ! y || ! json_is_number(y) || json_number_value(y) < 0)
                {
                    std::cerr << "Malformed field in element \"" << imageKey << "\".\n";
                    failed = true;
                    continue;
                }
                info.filename = imageKey;
                info.width  = json_number_value(w);
                info.height = json_number_value(h);
                info.channels = desc.channels;
                tile.x = json_number_value(x);
                tile.y = json_number_value(y);
                desc.images.push_back(info);
                desc.tiles.push_back(tile);
            }
        }

        if (!failed)
        {
            tilemaps.push_back(desc);
        }
    }

    json_decref(p);

    for (size_t i = 0; i < tilemaps.size(); ++i)
    {
        RenderTilemap(tilemaps[i]);
    }
}

void BlitImage(unsigned char const* src, int srcWidth, int srcHeight, unsigned char* dst, int dstX, int dstY, int dstPitch, int channels);

void RenderTilemap(TilemapDesc desc)
{
    if (desc.images.size() != desc.tiles.size() || desc.width == 0 || desc.height == 0 || desc.channels < 1 || desc.channels > 4)
    {
        std::cerr << "Malformed tilemap description\n";
        return;
    }

    std::cerr << "Rendering tilemap " << desc.filename << " with " << desc.images.size() << " images.\n";

    std::vector<unsigned char> pixelData(desc.width*desc.height*desc.channels);
    for (size_t i = 0; i < desc.images.size(); ++i)
    {
        ImageInfo const& info = desc.images[i];
        TileInfo const& tile = desc.tiles[i];
        int w, h, n;
        unsigned char* img = stbi_load(info.filename.c_str(), &w, &h, &n, desc.channels);
        if (! img)
        {
            std::cerr << "Image \"" << info.filename << "\" is missing or unreadable.\n";
            continue;
        }
        if (w != info.width || h != info.height)
        {
            std::cerr << "Image \"" << info.filename << "\" has size " << w << "x" << h
                << ", expected " << info.width << "x" << info.height << ".\n";
            continue;
        }
        if (tile.x + info.width > desc.width || tile.y + info.height > desc.height)
        {
            std::cerr << tile.x << " " << info.width  << " " << desc.width  << "\n";
            std::cerr << tile.y << " " << info.height << " " << desc.height << "\n";
            std::cerr << "Image \"" << info.filename << "\" extends outside the tilemap area.\n";
            continue;
        }
        BlitImage(img, info.width, info.height,
                &pixelData[0], tile.x, tile.y, desc.width*desc.channels, desc.channels);
        stbi_image_free(img);
    }
    stbi_write_png(desc.filename.c_str(), desc.width, desc.height, desc.channels,
            &pixelData[0], 0);
}

void BlitImage(unsigned char const* src, int srcWidth, int srcHeight, unsigned char* dst, int dstX, int dstY, int dstPitch, int channels)
{
    unsigned char r = rand(), g = rand(), b = rand(), a = 255;
    int srcPitch = srcWidth*channels;
    for (int row = 0; row < srcHeight; ++row)
    {
        unsigned char const* p = src + row*srcPitch;
        unsigned char* q = dst + channels*dstX + (row+dstY)*dstPitch;
        std::copy(p, p + srcPitch, q);
        if (debugTint)
        {
            for (int c = 0; c < srcPitch;)
            {
                q[c] = (r+q[c])/2; ++c;
                q[c] = (g+q[c])/2; ++c;
                q[c] = (b+q[c])/2; ++c;
                if (channels > 3)
                {
                    q[c] = a; ++c;
                }
            }
        }
    }
}

void Usage()
{
}
