Katla is licensed under the Boost Software License (LICENSE_1_0.txt).

This project contains third party software:
Jansson
Copyright (c) 2009-2012 Petri Lehtinen <petri@digip.org>
Licensed under the MIT license (Jansson/LICENSE).

stb_image and stb_image_write
Public domain (nothings.org)

RectangleBinPack
Public Domain (clb)
